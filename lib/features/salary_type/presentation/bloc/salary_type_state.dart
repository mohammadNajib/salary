part of 'salary_type_bloc.dart';

@immutable
abstract class SalaryTypeState {}

class SalaryTypeInitial extends SalaryTypeState {}

class SalaryTypeLoading extends SalaryTypeState {}

class SalaryTypeLoaded extends SalaryTypeState {
  final List<SalaryTypeEntity> salaryTypes;

  SalaryTypeLoaded({required this.salaryTypes});
}

class SalaryTypeError extends SalaryTypeState {
  final String message;

  SalaryTypeError({required this.message});
}

class SalaryTypeHandled extends SalaryTypeState {
  final String message;

  SalaryTypeHandled({required this.message});
}
