import 'package:dartz/dartz.dart';
import 'package:salary/features/salary_type/domain/entities/salary_type_entity.dart';

import '../../../../core/core_importer.dart';
import '../../domain/use_cases/salary_types_use_cases.dart';

part 'salary_type_event.dart';
part 'salary_type_state.dart';

class SalaryTypeBloc extends Bloc<SalaryTypeEvent, SalaryTypeState> {
  final SalaryTypesUseCases salaryTypesUseCases;
  SalaryTypeBloc({required this.salaryTypesUseCases}) : super(SalaryTypeInitial()) {
    on<SalaryTypeEvent>((event, emit) async {
      if (event is CreateSalaryTypeEvent) {
        emit(SalaryTypeLoading());
        Either either = await salaryTypesUseCases.createSalaryTypeUseCase(salaryTypeEntity: event.salaryTypeEntity);
        emit(either.fold((failure) => SalaryTypeError(message: failure.message ?? 'حدث خطأ'),
            (_) => SalaryTypeHandled(message: 'تم إضافة الصنف الجديد')));
      } else if (event is DeleteSalaryTypeEvent) {
        emit(SalaryTypeLoading());
        Either either = await salaryTypesUseCases.deleteSalaryTypeUseCase(salaryTypeId: event.salaryTypeId);
        emit(either.fold((failure) => SalaryTypeError(message: failure.message ?? 'حدث خطأ'),
            (_) => SalaryTypeHandled(message: 'تم حذف الصنف')));
      } else if (event is GetSalariesEvent) {
        emit(SalaryTypeLoading());
        Either either = await salaryTypesUseCases.getSalaryTypesUseCase();
        emit(either.fold((failure) => SalaryTypeError(message: failure.message ?? 'حدث خطأ'),
            (salaryTypes) => SalaryTypeLoaded(salaryTypes: salaryTypes)));
      } else if (event is UpdateSalaryTypeEvent) {
        emit(SalaryTypeLoading());
        Either either = await salaryTypesUseCases.updateSalaryTypeUseCase(salaryTypeEntity: event.salaryTypeEntity);
        emit(either.fold((failure) => SalaryTypeError(message: failure.message ?? 'حدث خطأ'),
            (_) => SalaryTypeHandled(message: 'تم تعديل الصنف')));
      }
    });
  }
}
