part of 'salary_type_bloc.dart';

@immutable
abstract class SalaryTypeEvent {}

class CreateSalaryTypeEvent extends SalaryTypeEvent {
  final SalaryTypeEntity salaryTypeEntity;

  CreateSalaryTypeEvent({required this.salaryTypeEntity});
}

class DeleteSalaryTypeEvent extends SalaryTypeEvent {
  final int salaryTypeId;

  DeleteSalaryTypeEvent({required this.salaryTypeId});
}

class UpdateSalaryTypeEvent extends SalaryTypeEvent {
  final SalaryTypeEntity salaryTypeEntity;

  UpdateSalaryTypeEvent({required this.salaryTypeEntity});
}

class GetSalariesEvent extends SalaryTypeEvent {}
