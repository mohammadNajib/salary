import 'package:dartz/dartz.dart';
import 'package:salary/features/salary_type/data/models/salary_type_model.dart';

abstract class SalaryTypeLocalDataSource {
  Future<List<SalaryTypeModel>> getSalaryTypes();
  Future<Unit> createSalaryType({required SalaryTypeModel salaryTypeModel});
  Future<Unit> updateSalaryType({required SalaryTypeModel salaryTypeModel});
  Future<Unit> deleteSalaryType({required int salaryTypeId});
}

class SalaryTypeLocalDataSourceImplement implements SalaryTypeLocalDataSource {
  @override
  Future<Unit> createSalaryType({required SalaryTypeModel salaryTypeModel}) {
    // TODO: implement createEmployee
    throw UnimplementedError();
  }

  @override
  Future<Unit> deleteSalaryType({required int salaryTypeId}) {
    // TODO: implement deleteEmployee
    throw UnimplementedError();
  }

  @override
  Future<List<SalaryTypeModel>> getSalaryTypes() {
    // TODO: implement getEmployee
    throw UnimplementedError();
  }

  @override
  Future<Unit> updateSalaryType({required SalaryTypeModel salaryTypeModel}) {
    // TODO: implement updateEmployee
    throw UnimplementedError();
  }
}
