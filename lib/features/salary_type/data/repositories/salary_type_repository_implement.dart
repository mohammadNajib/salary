import 'package:dartz/dartz.dart';
import 'package:salary/features/salary_type/data/models/salary_type_model.dart';
import 'package:salary/features/salary_type/domain/entities/salary_type_entity.dart';
import '../../../../core/core_importer.dart';
import '../../domain/repositories/salary_type_repository.dart';
import '../data_sources/salary_type_local_data_source.dart';

class SalaryTypeRepositoryImplement implements SalaryTypeRepository {
  final SalaryTypeLocalDataSource salaryTypeLocalDataSource;
  final RepositoryFactory repositoryFactory;
  final InternetConnectionCheckerPlus internetConnectionChecker;

  SalaryTypeRepositoryImplement({
    required this.internetConnectionChecker,
    required this.salaryTypeLocalDataSource,
    required this.repositoryFactory,
  });
  @override
  Future<Either<Failure, Unit>> createSalaryType({required SalaryTypeEntity salaryTypeEntity}) async {
    return await repositoryFactory.failureUnitRepo(
        function: () =>
            salaryTypeLocalDataSource.createSalaryType(salaryTypeModel: SalaryTypeModel(name: salaryTypeEntity.name)));
  }

  @override
  Future<Either<Failure, Unit>> deleteSalaryType({required int salaryTypeId}) async {
    return await repositoryFactory.failureUnitRepo(
        function: () => salaryTypeLocalDataSource.deleteSalaryType(salaryTypeId: salaryTypeId));
  }

  @override
  Future<Either<Failure, List<SalaryTypeEntity>>> getSalaryTypes() async {
    if (!await internetConnectionChecker.hasConnection) return Left(OfflineFailure());
    try {
      List<SalaryTypeModel> salaries = await salaryTypeLocalDataSource.getSalaryTypes();
      return Right(salaries);
    } on ServerException {
      return Left(ServerFailure());
    } on OfflineException {
      return Left(OfflineFailure());
    } catch (e) {
      return Left(InternalFailure());
    }
  }

  @override
  Future<Either<Failure, Unit>> updateSalaryType({required SalaryTypeEntity salaryTypeEntity}) async {
    return await repositoryFactory.failureUnitRepo(
        function: () => salaryTypeLocalDataSource.updateSalaryType(
            salaryTypeModel: SalaryTypeModel(id: salaryTypeEntity.id, name: salaryTypeEntity.name)));
  }
}
