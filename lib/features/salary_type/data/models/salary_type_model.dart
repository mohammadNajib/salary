import 'package:salary/features/salary_type/domain/entities/salary_type_entity.dart';

class SalaryTypeModel extends SalaryTypeEntity {
  SalaryTypeModel({super.id, super.name});
}
