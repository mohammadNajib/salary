import 'package:dartz/dartz.dart';
import 'package:salary/features/salary_type/domain/entities/salary_type_entity.dart';
import '../../../../core/core_importer.dart';

abstract class SalaryTypeRepository {
  Future<Either<Failure, List<SalaryTypeEntity>>> getSalaryTypes();
  Future<Either<Failure, Unit>> createSalaryType({required SalaryTypeEntity salaryTypeEntity});
  Future<Either<Failure, Unit>> updateSalaryType({required SalaryTypeEntity salaryTypeEntity});
  Future<Either<Failure, Unit>> deleteSalaryType({required int salaryTypeId});
}
