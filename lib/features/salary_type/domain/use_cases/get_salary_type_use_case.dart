import 'package:dartz/dartz.dart';
import 'package:salary/features/salary_type/domain/entities/salary_type_entity.dart';
import 'package:salary/features/salary_type/domain/repositories/salary_type_repository.dart';

import '../../../../core/core_importer.dart';

class GetSalaryTypesUseCase {
  final SalaryTypeRepository salaryTypeRepository;

  GetSalaryTypesUseCase({required this.salaryTypeRepository});

  Future<Either<Failure, List<SalaryTypeEntity>>> call() async {
    return await salaryTypeRepository.getSalaryTypes();
  }
}
