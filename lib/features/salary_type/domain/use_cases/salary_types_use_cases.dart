import 'create_salary_type_use_case.dart';
import 'delete_salary_type_use_case.dart';
import 'get_salary_type_use_case.dart';
import 'update_salary_type_use_case.dart';

class SalaryTypesUseCases {
  final CreateSalaryTypeUseCase createSalaryTypeUseCase;
  final DeleteSalaryTypeUseCase deleteSalaryTypeUseCase;
  final GetSalaryTypesUseCase getSalaryTypesUseCase;
  final UpdateSalaryTypeUseCase updateSalaryTypeUseCase;

  SalaryTypesUseCases({
    required this.createSalaryTypeUseCase,
    required this.deleteSalaryTypeUseCase,
    required this.getSalaryTypesUseCase,
    required this.updateSalaryTypeUseCase,
  });
}
