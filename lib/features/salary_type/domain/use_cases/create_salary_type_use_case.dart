import 'package:dartz/dartz.dart';
import 'package:salary/features/salary_type/domain/entities/salary_type_entity.dart';
import 'package:salary/features/salary_type/domain/repositories/salary_type_repository.dart';
import '../../../../core/core_importer.dart';

class CreateSalaryTypeUseCase {
  final SalaryTypeRepository salaryTypeRepository;

  CreateSalaryTypeUseCase({required this.salaryTypeRepository});

  Future<Either<Failure, Unit>> call({required SalaryTypeEntity salaryTypeEntity}) async {
    return await salaryTypeRepository.createSalaryType(salaryTypeEntity: salaryTypeEntity);
  }
}
