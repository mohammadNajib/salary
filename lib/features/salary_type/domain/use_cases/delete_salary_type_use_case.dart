import 'package:dartz/dartz.dart';
import 'package:salary/features/salary_type/domain/repositories/salary_type_repository.dart';

import '../../../../core/core_importer.dart';

class DeleteSalaryTypeUseCase {
  final SalaryTypeRepository salaryTypeRepository;

  DeleteSalaryTypeUseCase({required this.salaryTypeRepository});

  Future<Either<Failure, Unit>> call({required int salaryTypeId}) async {
    return await salaryTypeRepository.deleteSalaryType(salaryTypeId: salaryTypeId);
  }
}
