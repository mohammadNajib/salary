class SalaryTypeEntity {
  final int? id;
  final String? name;

  SalaryTypeEntity({this.id, this.name});
}
