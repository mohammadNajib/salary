import 'package:dartz/dartz.dart';
import 'package:salary/features/sections/domain/entities/section_entity.dart';
import '../../../../core/core_importer.dart';
import '../../domain/repositories/sections_repository.dart';
import '../data_sources/sections_local_data_source.dart';
import '../models/section_model.dart';

class SectionRepositoryImplement implements SectionRepository {
  final SectionsLocalDataSource sectionsLocalDataSource;
  final RepositoryFactory repositoryFactory;
  final InternetConnectionCheckerPlus internetConnectionChecker;

  SectionRepositoryImplement({
    required this.internetConnectionChecker,
    required this.sectionsLocalDataSource,
    required this.repositoryFactory,
  });
  @override
  Future<Either<Failure, Unit>> createSection({required SectionEntity sectionEntity}) async {
    return await repositoryFactory.failureUnitRepo(
        function: () => sectionsLocalDataSource.createSection(sectionModel: SectionModel(name: sectionEntity.name)));
  }

  @override
  Future<Either<Failure, Unit>> deleteSection({required int sectionId}) async {
    return await repositoryFactory.failureUnitRepo(
        function: () => sectionsLocalDataSource.deleteSection(sectionId: sectionId));
  }

  @override
  Future<Either<Failure, List<SectionEntity>>> getSection() async {
    if (!await internetConnectionChecker.hasConnection) return Left(OfflineFailure());
    try {
      List<SectionModel> sections = await sectionsLocalDataSource.getSections();
      return Right(sections);
    } on ServerException {
      return Left(ServerFailure());
    } on OfflineException {
      return Left(OfflineFailure());
    } catch (e) {
      return Left(InternalFailure());
    }
  }

  @override
  Future<Either<Failure, Unit>> updateSection({required SectionEntity sectionEntity}) async {
    return await repositoryFactory.failureUnitRepo(
        function: () => sectionsLocalDataSource.updateSection(
            sectionModel: SectionModel(name: sectionEntity.name, id: sectionEntity.id)));
  }
}
