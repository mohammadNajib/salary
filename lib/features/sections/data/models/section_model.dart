import 'package:salary/features/sections/domain/entities/section_entity.dart';

class SectionModel extends SectionEntity {
  SectionModel({super.id, super.name});
}
