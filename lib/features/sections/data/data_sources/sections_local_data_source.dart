import 'package:dartz/dartz.dart';
import 'package:salary/features/sections/data/models/section_model.dart';

abstract class SectionsLocalDataSource {
  Future<List<SectionModel>> getSections();
  Future<Unit> createSection({required SectionModel sectionModel});
  Future<Unit> updateSection({required SectionModel sectionModel});
  Future<Unit> deleteSection({required int sectionId});
}

class SectionsLocalDataSourceImplement implements SectionsLocalDataSource {
  @override
  Future<Unit> createSection({required SectionModel sectionModel}) {
    // TODO: implement createSection
    throw UnimplementedError();
  }

  @override
  Future<Unit> deleteSection({required int sectionId}) {
    // TODO: implement deleteSection
    throw UnimplementedError();
  }

  @override
  Future<List<SectionModel>> getSections() {
    // TODO: implement getSection
    throw UnimplementedError();
  }

  @override
  Future<Unit> updateSection({required SectionModel sectionModel}) {
    // TODO: implement updateSection
    throw UnimplementedError();
  }
}
