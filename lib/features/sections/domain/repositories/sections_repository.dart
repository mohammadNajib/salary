import 'package:dartz/dartz.dart';
import '../../../../core/core_importer.dart';
import '../entities/section_entity.dart';

abstract class SectionRepository {
  Future<Either<Failure, List<SectionEntity>>> getSection();
  Future<Either<Failure, Unit>> createSection({required SectionEntity sectionEntity});
  Future<Either<Failure, Unit>> updateSection({required SectionEntity sectionEntity});
  Future<Either<Failure, Unit>> deleteSection({required int sectionId});
}
