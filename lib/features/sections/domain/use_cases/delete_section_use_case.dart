import 'package:dartz/dartz.dart';
import '../../../../core/core_importer.dart';
import '../repositories/sections_repository.dart';

class DeleteSectionUseCase {
  final SectionRepository sectionRepository;

  DeleteSectionUseCase({required this.sectionRepository});

  Future<Either<Failure, Unit>> call({required int sectionId}) async {
    return await sectionRepository.deleteSection(sectionId: sectionId);
  }
}
