import 'package:dartz/dartz.dart';
import '../../../../core/core_importer.dart';
import '../entities/section_entity.dart';
import '../repositories/sections_repository.dart';

class UpdateSectionUseCase {
  final SectionRepository sectionRepository;

  UpdateSectionUseCase({required this.sectionRepository});

  Future<Either<Failure, Unit>> call({required SectionEntity sectionEntity}) async {
    return await sectionRepository.updateSection(sectionEntity: sectionEntity);
  }
}
