import 'package:salary/features/sections/domain/use_cases/create_section_use_case.dart';
import 'package:salary/features/sections/domain/use_cases/delete_section_use_case.dart';
import 'package:salary/features/sections/domain/use_cases/get_section_use_case.dart';
import 'package:salary/features/sections/domain/use_cases/update_section_use_case.dart';

class SectionUseCases {
  final CreateSectionUseCase createSectionUseCase;
  final DeleteSectionUseCase deleteSectionUseCase;
  final GetSectionUseCase getSectionUseCase;
  final UpdateSectionUseCase updateSectionUseCase;

  SectionUseCases({
    required this.createSectionUseCase,
    required this.deleteSectionUseCase,
    required this.getSectionUseCase,
    required this.updateSectionUseCase,
  });
}
