import 'package:dartz/dartz.dart';
import '../../../../core/core_importer.dart';
import '../entities/section_entity.dart';
import '../repositories/sections_repository.dart';

class GetSectionUseCase {
  final SectionRepository sectionRepository;

  GetSectionUseCase({required this.sectionRepository});

  Future<Either<Failure, List<SectionEntity>>> call() async {
    return await sectionRepository.getSection();
  }
}
