class SectionEntity {
  final int? id;
  final String? name;

  SectionEntity({this.id, this.name});
}
