part of 'sections_bloc.dart';

@immutable
abstract class SectionsEvent {}

class CreateSectionEvent extends SectionsEvent {
  final SectionEntity sectionEntity;

  CreateSectionEvent({required this.sectionEntity});
}

class DeleteSectionEvent extends SectionsEvent {
  final int sectionId;

  DeleteSectionEvent({required this.sectionId});
}

class UpdateSectionEvent extends SectionsEvent {
  final SectionEntity sectionEntity;

  UpdateSectionEvent({required this.sectionEntity});
}

class GetSectionsEvent extends SectionsEvent {}
