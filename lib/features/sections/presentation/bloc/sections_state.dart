part of 'sections_bloc.dart';

@immutable
abstract class SectionsState {}

class SectionsInitial extends SectionsState {}

class SectionLoading extends SectionsState {}

class SectionsLoaded extends SectionsState {
  final List<SectionEntity> sections;

  SectionsLoaded({required this.sections});
}

class SectionError extends SectionsState {
  final String message;

  SectionError({required this.message});
}

class SectionHandled extends SectionsState {
  final String message;

  SectionHandled({required this.message});
}
