import 'package:dartz/dartz.dart';

import '../../../../core/core_importer.dart';
import '../../domain/entities/section_entity.dart';
import '../../domain/use_cases/section_use_cases.dart';

part 'sections_event.dart';
part 'sections_state.dart';

class SectionsBloc extends Bloc<SectionsEvent, SectionsState> {
  final SectionUseCases sectionUseCases;
  SectionsBloc({required this.sectionUseCases}) : super(SectionsInitial()) {
    on<SectionsEvent>((event, emit) async {
      if (event is CreateSectionEvent) {
        emit(SectionLoading());
        Either either = await sectionUseCases.createSectionUseCase(sectionEntity: event.sectionEntity);
        emit(either.fold((failure) => SectionError(message: failure.message ?? 'حدث خطأ'),
            (_) => SectionHandled(message: 'تم إضافة القسم الجديد')));
      } else if (event is DeleteSectionEvent) {
        emit(SectionLoading());
        Either either = await sectionUseCases.deleteSectionUseCase(sectionId: event.sectionId);
        emit(either.fold((failure) => SectionError(message: failure.message ?? 'حدث خطأ'),
            (_) => SectionHandled(message: 'تم حذف القسم')));
      } else if (event is GetSectionsEvent) {
        emit(SectionLoading());
        Either either = await sectionUseCases.getSectionUseCase();
        emit(either.fold((failure) => SectionError(message: failure.message ?? 'حدث خطأ'),
            (sections) => SectionsLoaded(sections: sections)));
      } else if (event is UpdateSectionEvent) {
        emit(SectionLoading());
        Either either = await sectionUseCases.updateSectionUseCase(sectionEntity: event.sectionEntity);
        emit(either.fold((failure) => SectionError(message: failure.message ?? 'حدث خطأ'),
            (_) => SectionHandled(message: 'تم تعديل القسم')));
      }
    });
  }
}
