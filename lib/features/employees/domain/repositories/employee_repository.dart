import 'package:dartz/dartz.dart';

import '../../../../core/core_importer.dart';
import '../entities/employee_entity.dart';

abstract class EmployeeRepository {
  Future<Either<Failure, List<EmployeeEntity>>> getEmployee();
  Future<Either<Failure, Unit>> createEmployee({required EmployeeEntity employeeEntity});
  Future<Either<Failure, Unit>> updateEmployee({required EmployeeEntity employeeEntity});
  Future<Either<Failure, Unit>> deleteEmployee({required int employeeId});
}
