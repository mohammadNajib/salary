import 'package:dartz/dartz.dart';
import 'package:salary/features/employees/domain/repositories/employee_repository.dart';

import '../../../../core/core_importer.dart';
import '../entities/employee_entity.dart';

class CreateEmployeeUseCase {
  final EmployeeRepository employeeRepository;

  CreateEmployeeUseCase({required this.employeeRepository});

  Future<Either<Failure, Unit>> call({required EmployeeEntity employeeEntity}) async {
    return await employeeRepository.createEmployee(employeeEntity: employeeEntity);
  }
}
