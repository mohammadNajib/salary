import 'package:dartz/dartz.dart';
import 'package:salary/features/employees/domain/repositories/employee_repository.dart';

import '../../../../core/core_importer.dart';
import '../entities/employee_entity.dart';

class GetEmployeeUseCase {
  final EmployeeRepository employeeRepository;

  GetEmployeeUseCase({required this.employeeRepository});

  Future<Either<Failure, List<EmployeeEntity>>> call() async {
    return await employeeRepository.getEmployee();
  }
}
