import 'package:salary/features/employees/domain/use_cases/create_employee_use_case.dart';
import 'package:salary/features/employees/domain/use_cases/delete_employee_use_case.dart';
import 'package:salary/features/employees/domain/use_cases/get_employee_use_case.dart';
import 'package:salary/features/employees/domain/use_cases/update_employee_use_case.dart';

class EmployeeUseCases {
  final CreateEmployeeUseCase createEmployeeUseCase;
  final DeleteEmployeeUseCase deleteEmployeeUseCase;
  final GetEmployeeUseCase getEmployeeUseCase;
  final UpdateEmployeeUseCase updateEmployeeUseCase;

  EmployeeUseCases({
    required this.createEmployeeUseCase,
    required this.deleteEmployeeUseCase,
    required this.getEmployeeUseCase,
    required this.updateEmployeeUseCase,
  });
}
