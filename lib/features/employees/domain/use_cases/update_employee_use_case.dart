import 'package:dartz/dartz.dart';
import 'package:salary/features/employees/domain/repositories/employee_repository.dart';

import '../../../../core/core_importer.dart';
import '../entities/employee_entity.dart';

class UpdateEmployeeUseCase {
  final EmployeeRepository employeeRepository;

  UpdateEmployeeUseCase({required this.employeeRepository});

  Future<Either<Failure, Unit>> call({required EmployeeEntity employeeEntity}) async {
    return await employeeRepository.updateEmployee(employeeEntity: employeeEntity);
  }
}
