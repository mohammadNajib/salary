import 'package:dartz/dartz.dart';
import 'package:salary/features/employees/domain/repositories/employee_repository.dart';

import '../../../../core/core_importer.dart';

class DeleteEmployeeUseCase {
  final EmployeeRepository employeeRepository;

  DeleteEmployeeUseCase({required this.employeeRepository});

  Future<Either<Failure, Unit>> call({required int employeeId}) async {
    return await employeeRepository.deleteEmployee(employeeId: employeeId);
  }
}
