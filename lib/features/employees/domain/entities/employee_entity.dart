class EmployeeEntity {
  final int? id;
  final String name;
  final int salaryAmount;
  final bool isFullTime;
  final int salaryTypeId;
  final int workingHours;
  final String phoneNumber;
  final DateTime birthDate;
  final int sectionId;

  EmployeeEntity({
    this.id,
    required this.name,
    required this.salaryAmount,
    required this.isFullTime,
    required this.salaryTypeId,
    required this.workingHours,
    required this.phoneNumber,
    required this.birthDate,
    required this.sectionId,
  });
}
