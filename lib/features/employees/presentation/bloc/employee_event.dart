part of 'employee_bloc.dart';

@immutable
abstract class EmployeeEvent {}

class CreateEmployeeEvent extends EmployeeEvent {
  final EmployeeEntity employeeEntity;

  CreateEmployeeEvent({required this.employeeEntity});
}

class DeleteEmployeeEvent extends EmployeeEvent {
  final int employeeId;

  DeleteEmployeeEvent({required this.employeeId});
}

class UpdateEmployeeEvent extends EmployeeEvent {
  final EmployeeEntity employeeEntity;

  UpdateEmployeeEvent({required this.employeeEntity});
}

class GetEmployeesEvent extends EmployeeEvent {}
