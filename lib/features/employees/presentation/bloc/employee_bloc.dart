import 'package:dartz/dartz.dart';
import 'package:salary/features/employees/domain/entities/employee_entity.dart';
import 'package:salary/features/employees/domain/use_cases/employee_use_cases.dart';

import '../../../../core/core_importer.dart';

part 'employee_event.dart';
part 'employee_state.dart';

class EmployeeBloc extends Bloc<EmployeeEvent, EmployeeState> {
  final EmployeeUseCases employeeUseCases;
  EmployeeBloc({required this.employeeUseCases}) : super(EmployeeInitial()) {
    on<EmployeeEvent>((event, emit) async {
      if (event is CreateEmployeeEvent) {
        emit(EmployeeLoading());
        Either either = await employeeUseCases.createEmployeeUseCase(employeeEntity: event.employeeEntity);
        emit(either.fold((failure) => EmployeeError(message: failure.message ?? 'حدث خطأ'),
            (_) => EmployeeHandled(message: 'تم إضافة الموظف الجديد')));
      } else if (event is DeleteEmployeeEvent) {
        emit(EmployeeLoading());
        Either either = await employeeUseCases.deleteEmployeeUseCase(employeeId: event.employeeId);
        emit(either.fold((failure) => EmployeeError(message: failure.message ?? 'حدث خطأ'),
            (_) => EmployeeHandled(message: 'تم حذف الموظف')));
      } else if (event is GetEmployeesEvent) {
        emit(EmployeeLoading());
        Either either = await employeeUseCases.getEmployeeUseCase();
        emit(either.fold((failure) => EmployeeError(message: failure.message ?? 'حدث خطأ'),
            (employees) => EmployeesLoaded(employees: employees)));
      } else if (event is UpdateEmployeeEvent) {
        emit(EmployeeLoading());
        Either either = await employeeUseCases.updateEmployeeUseCase(employeeEntity: event.employeeEntity);
        emit(either.fold((failure) => EmployeeError(message: failure.message ?? 'حدث خطأ'),
            (_) => EmployeeHandled(message: 'تم تعديل الموظف')));
      }
    });
  }
}
