part of 'employee_bloc.dart';

@immutable
abstract class EmployeeState {}

class EmployeeInitial extends EmployeeState {}

class EmployeeLoading extends EmployeeState {}

class EmployeesLoaded extends EmployeeState {
  final List<EmployeeEntity> employees;

  EmployeesLoaded({required this.employees});
}

class EmployeeError extends EmployeeState {
  final String message;

  EmployeeError({required this.message});
}

class EmployeeHandled extends EmployeeState {
  final String message;

  EmployeeHandled({required this.message});
}
