import 'package:dartz/dartz.dart';
import 'package:salary/features/employees/data/data_sources/employee_local_data_source.dart';
import 'package:salary/features/employees/domain/entities/employee_entity.dart';
import 'package:salary/features/employees/domain/repositories/employee_repository.dart';

import '../../../../core/core_importer.dart';
import '../models/employee_model.dart';

class EmployeeRepositoryImplement implements EmployeeRepository {
  final EmployeeLocalDataSource employeeLocalDataSource;
  final RepositoryFactory repositoryFactory;
  final InternetConnectionCheckerPlus internetConnectionChecker;

  EmployeeRepositoryImplement({
    required this.internetConnectionChecker,
    required this.employeeLocalDataSource,
    required this.repositoryFactory,
  });
  @override
  Future<Either<Failure, Unit>> createEmployee({required EmployeeEntity employeeEntity}) async {
    return await repositoryFactory.failureUnitRepo(
        function: () => employeeLocalDataSource.createEmployee(
            employeeModel: EmployeeModel.entityToModel(employeeEntity: employeeEntity)));
  }

  @override
  Future<Either<Failure, Unit>> deleteEmployee({required int employeeId}) async {
    return await repositoryFactory.failureUnitRepo(
        function: () => employeeLocalDataSource.deleteEmployee(employeeId: employeeId));
  }

  @override
  Future<Either<Failure, List<EmployeeEntity>>> getEmployee() async {
    if (!await internetConnectionChecker.hasConnection) return Left(OfflineFailure());
    try {
      List<EmployeeModel> employees = await employeeLocalDataSource.getEmployees();
      return Right(employees);
    } on ServerException {
      return Left(ServerFailure());
    } on OfflineException {
      return Left(OfflineFailure());
    } catch (e) {
      return Left(InternalFailure());
    }
  }

  @override
  Future<Either<Failure, Unit>> updateEmployee({required EmployeeEntity employeeEntity}) async {
    return await repositoryFactory.failureUnitRepo(
        function: () => employeeLocalDataSource.updateEmployee(
            employeeModel: EmployeeModel.entityToModel(employeeEntity: employeeEntity)));
  }
}
