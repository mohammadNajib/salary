import 'package:dartz/dartz.dart';
import 'package:salary/features/employees/data/models/employee_model.dart';

abstract class EmployeeLocalDataSource {
  Future<List<EmployeeModel>> getEmployees();
  Future<Unit> createEmployee({required EmployeeModel employeeModel});
  Future<Unit> updateEmployee({required EmployeeModel employeeModel});
  Future<Unit> deleteEmployee({required int employeeId});
}

class EmployeeLocalDataSourceImplement implements EmployeeLocalDataSource {
  @override
  Future<Unit> createEmployee({required EmployeeModel employeeModel}) {
    // TODO: implement createEmployee
    throw UnimplementedError();
  }

  @override
  Future<Unit> deleteEmployee({required int employeeId}) {
    // TODO: implement deleteEmployee
    throw UnimplementedError();
  }

  @override
  Future<List<EmployeeModel>> getEmployees() {
    // TODO: implement getEmployee
    throw UnimplementedError();
  }

  @override
  Future<Unit> updateEmployee({required EmployeeModel employeeModel}) {
    // TODO: implement updateEmployee
    throw UnimplementedError();
  }
}
