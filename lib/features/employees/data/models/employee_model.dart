import 'package:salary/features/employees/domain/entities/employee_entity.dart';

class EmployeeModel extends EmployeeEntity {
  EmployeeModel({
    required super.name,
    required super.salaryAmount,
    required super.isFullTime,
    required super.salaryTypeId,
    required super.workingHours,
    required super.phoneNumber,
    required super.birthDate,
    required super.sectionId,
  });
  static EmployeeModel entityToModel({required EmployeeEntity employeeEntity}) {
    return EmployeeModel(
      name: employeeEntity.name,
      salaryAmount: employeeEntity.salaryAmount,
      isFullTime: employeeEntity.isFullTime,
      salaryTypeId: employeeEntity.salaryTypeId,
      workingHours: employeeEntity.workingHours,
      phoneNumber: employeeEntity.phoneNumber,
      birthDate: employeeEntity.birthDate,
      sectionId: employeeEntity.sectionId,
    );
  }
}
