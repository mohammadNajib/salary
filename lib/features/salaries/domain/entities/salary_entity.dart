class SalaryEntity {
  final int? id;
  final int employeeId;
  final int monthBalance;
  final int monthSalary;
  final DateTime salaryDate;
  final int extra;
  final double absentHours;
  final double absentDiscount;

  final int balancePayment;

  SalaryEntity({
    this.id,
    required this.employeeId,
    required this.monthBalance,
    required this.monthSalary,
    required this.salaryDate,
    required this.extra,
    required this.absentHours,
    required this.absentDiscount,
    required this.balancePayment,
  });
}
