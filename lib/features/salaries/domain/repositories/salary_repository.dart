import 'package:dartz/dartz.dart';
import 'package:salary/features/salaries/domain/entities/salary_entity.dart';
import '../../../../core/core_importer.dart';

abstract class SalaryRepository {
  Future<Either<Failure, List<SalaryEntity>>> getSalaries();
  Future<Either<Failure, Unit>> createSalary({required SalaryEntity salaryEntity});
  Future<Either<Failure, Unit>> updateSalary({required SalaryEntity salaryEntity});
  Future<Either<Failure, Unit>> deleteSalary({required int salaryId});
}
