import 'package:dartz/dartz.dart';
import 'package:salary/features/salaries/domain/repositories/salary_repository.dart';

import '../../../../core/core_importer.dart';

class DeleteSalaryUseCase {
  final SalaryRepository salaryRepository;

  DeleteSalaryUseCase({required this.salaryRepository});

  Future<Either<Failure, Unit>> call({required int salaryId}) async {
    return await salaryRepository.deleteSalary(salaryId: salaryId);
  }
}
