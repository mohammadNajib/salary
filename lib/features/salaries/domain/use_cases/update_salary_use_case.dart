import 'package:dartz/dartz.dart';
import 'package:salary/features/salaries/domain/entities/salary_entity.dart';
import 'package:salary/features/salaries/domain/repositories/salary_repository.dart';
import '../../../../core/core_importer.dart';

class UpdateSalaryUseCase {
  final SalaryRepository salaryRepository;

  UpdateSalaryUseCase({required this.salaryRepository});

  Future<Either<Failure, Unit>> call({required SalaryEntity salaryEntity}) async {
    return await salaryRepository.updateSalary(salaryEntity: salaryEntity);
  }
}
