import 'package:dartz/dartz.dart';
import 'package:salary/features/salaries/domain/entities/salary_entity.dart';
import 'package:salary/features/salaries/domain/repositories/salary_repository.dart';

import '../../../../core/core_importer.dart';

class GetSalariesUseCase {
  final SalaryRepository salaryRepository;

  GetSalariesUseCase({required this.salaryRepository});

  Future<Either<Failure, List<SalaryEntity>>> call() async {
    return await salaryRepository.getSalaries();
  }
}
