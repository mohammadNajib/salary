import 'create_salary_use_case.dart';
import 'delete_salary_use_case.dart';
import 'get_salary_use_case.dart';
import 'update_salary_use_case.dart';

class SalariesUseCases {
  final CreateSalaryUseCase createSalaryUseCase;
  final DeleteSalaryUseCase deleteSalaryUseCase;
  final GetSalariesUseCase getSalaryUseCase;
  final UpdateSalaryUseCase updateSalaryUseCase;

  SalariesUseCases({
    required this.createSalaryUseCase,
    required this.deleteSalaryUseCase,
    required this.getSalaryUseCase,
    required this.updateSalaryUseCase,
  });
}
