import 'package:dartz/dartz.dart';
import 'package:salary/features/salaries/data/models/salary_model.dart';
import 'package:salary/features/salaries/domain/entities/salary_entity.dart';
import '../../../../core/core_importer.dart';
import '../../domain/repositories/salary_repository.dart';
import '../data_sources/salaries_local_data_source.dart';

class SalaryRepositoryImplement implements SalaryRepository {
  final SalaryLocalDataSource salaryLocalDataSource;
  final RepositoryFactory repositoryFactory;
  final InternetConnectionCheckerPlus internetConnectionChecker;

  SalaryRepositoryImplement({
    required this.internetConnectionChecker,
    required this.salaryLocalDataSource,
    required this.repositoryFactory,
  });
  @override
  Future<Either<Failure, Unit>> createSalary({required SalaryEntity salaryEntity}) async {
    return await repositoryFactory.failureUnitRepo(
        function: () =>
            salaryLocalDataSource.createSalary(salaryModel: SalaryModel.entityToModel(salaryEntity: salaryEntity)));
  }

  @override
  Future<Either<Failure, Unit>> deleteSalary({required int salaryId}) async {
    return await repositoryFactory.failureUnitRepo(
        function: () => salaryLocalDataSource.deleteSalary(salaryId: salaryId));
  }

  @override
  Future<Either<Failure, List<SalaryEntity>>> getSalaries() async {
    if (!await internetConnectionChecker.hasConnection) return Left(OfflineFailure());
    try {
      List<SalaryModel> salaries = await salaryLocalDataSource.getSalaries();
      return Right(salaries);
    } on ServerException {
      return Left(ServerFailure());
    } on OfflineException {
      return Left(OfflineFailure());
    } catch (e) {
      return Left(InternalFailure());
    }
  }

  @override
  Future<Either<Failure, Unit>> updateSalary({required SalaryEntity salaryEntity}) async {
    return await repositoryFactory.failureUnitRepo(
        function: () =>
            salaryLocalDataSource.updateSalary(salaryModel: SalaryModel.entityToModel(salaryEntity: salaryEntity)));
  }
}
