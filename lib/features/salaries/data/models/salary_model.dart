import 'package:salary/features/salaries/domain/entities/salary_entity.dart';

class SalaryModel extends SalaryEntity {
  SalaryModel({
    required super.employeeId,
    required super.monthBalance,
    required super.monthSalary,
    required super.salaryDate,
    required super.extra,
    required super.absentHours,
    required super.absentDiscount,
    required super.balancePayment,
  });

  static SalaryModel entityToModel({required SalaryEntity salaryEntity}) {
    return SalaryModel(
      absentDiscount: salaryEntity.absentDiscount,
      absentHours: salaryEntity.absentHours,
      balancePayment: salaryEntity.balancePayment,
      employeeId: salaryEntity.employeeId,
      extra: salaryEntity.extra,
      monthBalance: salaryEntity.monthBalance,
      monthSalary: salaryEntity.monthSalary,
      salaryDate: salaryEntity.salaryDate,
    );
  }
}
