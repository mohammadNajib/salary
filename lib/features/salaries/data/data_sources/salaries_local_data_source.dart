import 'package:dartz/dartz.dart';
import 'package:salary/features/salaries/data/models/salary_model.dart';

abstract class SalaryLocalDataSource {
  Future<List<SalaryModel>> getSalaries();
  Future<Unit> createSalary({required SalaryModel salaryModel});
  Future<Unit> updateSalary({required SalaryModel salaryModel});
  Future<Unit> deleteSalary({required int salaryId});
}

class SalaryLocalDataSourceImplement implements SalaryLocalDataSource {
  @override
  Future<Unit> createSalary({required SalaryModel salaryModel}) {
    // TODO: implement createEmployee
    throw UnimplementedError();
  }

  @override
  Future<Unit> deleteSalary({required int salaryId}) {
    // TODO: implement deleteEmployee
    throw UnimplementedError();
  }

  @override
  Future<List<SalaryModel>> getSalaries() {
    // TODO: implement getEmployee
    throw UnimplementedError();
  }

  @override
  Future<Unit> updateSalary({required SalaryModel salaryModel}) {
    // TODO: implement updateEmployee
    throw UnimplementedError();
  }
}
