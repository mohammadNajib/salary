part of 'salary_bloc.dart';

@immutable
abstract class SalaryState {}

class SalaryInitial extends SalaryState {}

class SalaryLoading extends SalaryState {}

class SalariesLoaded extends SalaryState {
  final List<SalaryEntity> salaries;

  SalariesLoaded({required this.salaries});
}

class SalaryError extends SalaryState {
  final String message;

  SalaryError({required this.message});
}

class SalaryHandled extends SalaryState {
  final String message;

  SalaryHandled({required this.message});
}
