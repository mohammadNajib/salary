part of 'salary_bloc.dart';

@immutable
abstract class SalaryEvent {}

class CreateSalaryEvent extends SalaryEvent {
  final SalaryEntity salaryEntity;

  CreateSalaryEvent({required this.salaryEntity});
}

class DeleteSalaryEvent extends SalaryEvent {
  final int salaryId;

  DeleteSalaryEvent({required this.salaryId});
}

class UpdateSalaryEvent extends SalaryEvent {
  final SalaryEntity salaryEntity;

  UpdateSalaryEvent({required this.salaryEntity});
}

class GetSalariesEvent extends SalaryEvent {}
