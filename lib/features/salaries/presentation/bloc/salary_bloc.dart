import 'package:dartz/dartz.dart';
import 'package:salary/features/salaries/domain/use_cases/salaries_use_cases.dart';

import '../../../../core/core_importer.dart';
import '../../domain/entities/salary_entity.dart';

part 'salary_event.dart';
part 'salary_state.dart';

class SalaryBloc extends Bloc<SalaryEvent, SalaryState> {
  final SalariesUseCases salariesUseCases;
  SalaryBloc({required this.salariesUseCases}) : super(SalaryInitial()) {
    on<SalaryEvent>((event, emit) async {
      if (event is CreateSalaryEvent) {
        emit(SalaryLoading());
        Either either = await salariesUseCases.createSalaryUseCase(salaryEntity: event.salaryEntity);
        emit(either.fold((failure) => SalaryError(message: failure.message ?? 'حدث خطأ'),
            (_) => SalaryHandled(message: 'تم إضافة الراتب الجديد')));
      } else if (event is DeleteSalaryEvent) {
        emit(SalaryLoading());
        Either either = await salariesUseCases.deleteSalaryUseCase(salaryId: event.salaryId);
        emit(either.fold((failure) => SalaryError(message: failure.message ?? 'حدث خطأ'),
            (_) => SalaryHandled(message: 'تم حذف الراتب')));
      } else if (event is GetSalariesEvent) {
        emit(SalaryLoading());
        Either either = await salariesUseCases.getSalaryUseCase();
        emit(either.fold((failure) => SalaryError(message: failure.message ?? 'حدث خطأ'),
            (salaries) => SalariesLoaded(salaries: salaries)));
      } else if (event is UpdateSalaryEvent) {
        emit(SalaryLoading());
        Either either = await salariesUseCases.updateSalaryUseCase(salaryEntity: event.salaryEntity);
        emit(either.fold((failure) => SalaryError(message: failure.message ?? 'حدث خطأ'),
            (_) => SalaryHandled(message: 'تم تعديل الراتب')));
      }
    });
  }
}
