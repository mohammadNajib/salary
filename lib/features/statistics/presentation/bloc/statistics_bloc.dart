import 'package:salary/features/statistics/domain/use_cases/statistics_use_cases.dart';

import '../../../../core/core_importer.dart';

part 'statistics_event.dart';
part 'statistics_state.dart';

class StatisticsBloc extends Bloc<StatisticsEvent, StatisticsState> {
  final StatisticsUseCase statisticsUseCase;
  StatisticsBloc({required this.statisticsUseCase}) : super(StatisticsInitial()) {
    on<StatisticsEvent>((event, emit) {
      // TODO: implement event handler
    });
  }
}
