import '../../../../core/core_importer.dart';
import '../../domain/repositories/statistics_repository.dart';
import '../data_sources/statistics_local_data_source.dart';

class StatisticsRepositoryImplement implements StatisticsRepository {
  final StatisticsLocalDataSource sectionsLocalDataSource;
  final RepositoryFactory repositoryFactory;
  final InternetConnectionCheckerPlus internetConnectionChecker;

  StatisticsRepositoryImplement({
    required this.sectionsLocalDataSource,
    required this.repositoryFactory,
    required this.internetConnectionChecker,
  });
}
