import 'package:salary/features/sections/domain/use_cases/create_section_use_case.dart';
import 'package:salary/features/sections/domain/use_cases/delete_section_use_case.dart';
import 'package:salary/features/sections/domain/use_cases/get_section_use_case.dart';
import 'package:salary/features/sections/domain/use_cases/update_section_use_case.dart';
import '../core/core_importer.dart';
import '../features/sections/data/data_sources/sections_local_data_source.dart';
import '../features/sections/data/repositories/sections_repository_implement.dart';
import '../features/sections/domain/repositories/sections_repository.dart';
import '../features/sections/presentation/bloc/sections_bloc.dart';

Future<void> injectSection() async {
  sl.registerFactory(() => SectionsBloc(sectionUseCases: sl()));

  sl.registerLazySingleton(() => GetSectionUseCase(sectionRepository: sl()));
  sl.registerLazySingleton(() => CreateSectionUseCase(sectionRepository: sl()));
  sl.registerLazySingleton(() => DeleteSectionUseCase(sectionRepository: sl()));
  sl.registerLazySingleton(() => UpdateSectionUseCase(sectionRepository: sl()));

  sl.registerLazySingleton<SectionRepository>(() => SectionRepositoryImplement(
      internetConnectionChecker: sl(), repositoryFactory: sl(), sectionsLocalDataSource: sl()));

  sl.registerLazySingleton<SectionsLocalDataSource>(() => SectionsLocalDataSourceImplement());
}
