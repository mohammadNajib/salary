import '../core/core_importer.dart';
import '../features/salary_type/data/data_sources/salary_type_local_data_source.dart';
import '../features/salary_type/data/repositories/salary_type_repository_implement.dart';
import '../features/salary_type/domain/repositories/salary_type_repository.dart';
import '../features/salary_type/domain/use_cases/create_salary_type_use_case.dart';
import '../features/salary_type/domain/use_cases/delete_salary_type_use_case.dart';
import '../features/salary_type/domain/use_cases/get_salary_type_use_case.dart';
import '../features/salary_type/domain/use_cases/update_salary_type_use_case.dart';
import '../features/salary_type/presentation/bloc/salary_type_bloc.dart';

Future<void> injectSalaryTypes() async {
  sl.registerFactory(() => SalaryTypeBloc(salaryTypesUseCases: sl()));

  sl.registerLazySingleton(() => GetSalaryTypesUseCase(salaryTypeRepository: sl()));
  sl.registerLazySingleton(() => CreateSalaryTypeUseCase(salaryTypeRepository: sl()));
  sl.registerLazySingleton(() => DeleteSalaryTypeUseCase(salaryTypeRepository: sl()));
  sl.registerLazySingleton(() => UpdateSalaryTypeUseCase(salaryTypeRepository: sl()));

  sl.registerLazySingleton<SalaryTypeRepository>(() => SalaryTypeRepositoryImplement(
      internetConnectionChecker: sl(), repositoryFactory: sl(), salaryTypeLocalDataSource: sl()));

  sl.registerLazySingleton<SalaryTypeLocalDataSource>(() => SalaryTypeLocalDataSourceImplement());
}
