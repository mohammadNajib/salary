import 'package:salary/core/theme/bloc/theme_bloc.dart';

import '../core/core_importer.dart';

Future<void> injectTheme() async {
  sl.registerLazySingleton(() => ThemeCacheHelper(sharedPreferences: sl()));
  sl.registerFactory(() => ThemeBloc(themeCacheHelper: sl()));
}
