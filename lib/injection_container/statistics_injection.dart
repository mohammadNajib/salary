import 'package:salary/features/statistics/data/data_sources/statistics_local_data_source.dart';
import 'package:salary/features/statistics/data/repositories/statistics_repository_implement.dart';
import 'package:salary/features/statistics/domain/repositories/statistics_repository.dart';
import 'package:salary/features/statistics/presentation/bloc/statistics_bloc.dart';
import '../core/core_importer.dart';

Future<void> injectStatistics() async {
  sl.registerFactory(() => StatisticsBloc(statisticsUseCase: sl()));

  sl.registerLazySingleton<StatisticsRepository>(() => StatisticsRepositoryImplement(
      internetConnectionChecker: sl(), repositoryFactory: sl(), sectionsLocalDataSource: sl()));

  sl.registerLazySingleton<StatisticsLocalDataSource>(() => StatisticsLocalDataSourceImplement());
}
