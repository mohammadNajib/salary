import '../core/core_importer.dart';
import '../features/salaries/data/data_sources/salaries_local_data_source.dart';
import '../features/salaries/data/repositories/salary_repository_implement.dart';
import '../features/salaries/domain/repositories/salary_repository.dart';
import '../features/salaries/domain/use_cases/create_salary_use_case.dart';
import '../features/salaries/domain/use_cases/delete_salary_use_case.dart';
import '../features/salaries/domain/use_cases/get_salary_use_case.dart';
import '../features/salaries/domain/use_cases/update_salary_use_case.dart';
import '../features/salaries/presentation/bloc/salary_bloc.dart';

Future<void> injectSalaries() async {
  sl.registerFactory(() => SalaryBloc(salariesUseCases: sl()));

  sl.registerLazySingleton(() => GetSalariesUseCase(salaryRepository: sl()));
  sl.registerLazySingleton(() => CreateSalaryUseCase(salaryRepository: sl()));
  sl.registerLazySingleton(() => DeleteSalaryUseCase(salaryRepository: sl()));
  sl.registerLazySingleton(() => UpdateSalaryUseCase(salaryRepository: sl()));

  sl.registerLazySingleton<SalaryRepository>(() =>
      SalaryRepositoryImplement(internetConnectionChecker: sl(), repositoryFactory: sl(), salaryLocalDataSource: sl()));

  sl.registerLazySingleton<SalaryLocalDataSource>(() => SalaryLocalDataSourceImplement());
}
