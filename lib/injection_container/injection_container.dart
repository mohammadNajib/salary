import 'package:salary/injection_container/employee_injection.dart';
import 'package:salary/injection_container/salaries_injection.dart';
import 'package:salary/injection_container/section_injection.dart';
import 'package:salary/injection_container/statistics_injection.dart';
import 'package:salary/injection_container/theme_injection.dart';
import '../core/core_importer.dart';
import 'salary_types_injection.dart';

final sl = GetIt.instance;
Future<void> inject() async {
  sl.registerLazySingleton(() => InternetConnectionCheckerPlus());
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  sl.registerLazySingleton(() => sharedPreferences);

  await injectTheme();
  await injectEmployee();
  await injectSalaries();
  await injectSalaryTypes();
  await injectSection();
  await injectStatistics();
}
