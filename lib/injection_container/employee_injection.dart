import 'package:salary/features/employees/data/data_sources/employee_local_data_source.dart';
import 'package:salary/features/employees/data/repositories/employee_repository_implement.dart';
import 'package:salary/features/employees/domain/repositories/employee_repository.dart';
import 'package:salary/features/employees/domain/use_cases/create_employee_use_case.dart';
import 'package:salary/features/employees/domain/use_cases/delete_employee_use_case.dart';
import 'package:salary/features/employees/domain/use_cases/get_employee_use_case.dart';
import 'package:salary/features/employees/domain/use_cases/update_employee_use_case.dart';
import 'package:salary/features/employees/presentation/bloc/employee_bloc.dart';

import '../core/core_importer.dart';

Future<void> injectEmployee() async {
  sl.registerFactory(() => EmployeeBloc(employeeUseCases: sl()));

  sl.registerLazySingleton(() => GetEmployeeUseCase(employeeRepository: sl()));
  sl.registerLazySingleton(() => CreateEmployeeUseCase(employeeRepository: sl()));
  sl.registerLazySingleton(() => DeleteEmployeeUseCase(employeeRepository: sl()));
  sl.registerLazySingleton(() => UpdateEmployeeUseCase(employeeRepository: sl()));

  sl.registerLazySingleton<EmployeeRepository>(() => EmployeeRepositoryImplement(
      internetConnectionChecker: sl(), repositoryFactory: sl(), employeeLocalDataSource: sl()));

  sl.registerLazySingleton<EmployeeLocalDataSource>(() => EmployeeLocalDataSourceImplement());
}
