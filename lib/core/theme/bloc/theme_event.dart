part of 'theme_bloc.dart';

@immutable
abstract class ThemeEvent {}

class GetCurrentThemeEvent extends ThemeEvent {}

class ChangeTheme extends ThemeEvent {
  final AppTheme appTheme;

  ChangeTheme({required this.appTheme});
}
