import 'package:salary/core/core_importer.dart';

part 'theme_event.dart';
part 'theme_state.dart';

class ThemeBloc extends Bloc<ThemeEvent, ThemeState> {
  final ThemeCacheHelper themeCacheHelper;
  ThemeBloc({required this.themeCacheHelper}) : super(ThemeInitial()) {
    on<ThemeEvent>((event, emit) async {
      if (event is GetCurrentThemeEvent) {
        int index = await themeCacheHelper.getCachedThemeIndex();
        emit(LoadedThemeState(themeData: appThemeData[AppTheme.values[index]]!));
      } else if (event is ChangeTheme) {
        await themeCacheHelper.cacheThemeIndex(event.appTheme.index);
        emit(LoadedThemeState(themeData: appThemeData[event.appTheme]!));
      }
    });
  }
}
