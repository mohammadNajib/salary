import '../core_importer.dart';

class ThemeCacheHelper {
  final SharedPreferences sharedPreferences;

  ThemeCacheHelper({required this.sharedPreferences});

  Future<void> cacheThemeIndex(int index) async {
    sharedPreferences.setInt(themeIndexKey, index);
  }

  Future<int> getCachedThemeIndex() async {
    final cachedThemeIndex = sharedPreferences.getInt(themeIndexKey);
    return cachedThemeIndex ?? 0;
  }
}
