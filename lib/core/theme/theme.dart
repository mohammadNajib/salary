import '../core_importer.dart';

enum AppTheme {
  light('Light'),
  dark('Dark');

  final String? name;
  const AppTheme(this.name);
}

final appThemeData = {
  AppTheme.light: ThemeData(
    backgroundColor: Colors.white,
    textSelectionTheme: TextSelectionThemeData(cursorColor: primaryColor),
    appBarTheme: AppBarTheme(backgroundColor: primaryColor),
    brightness: Brightness.light,
    primaryColor: primaryColor,
  ),
  AppTheme.dark: ThemeData(
    backgroundColor: Colors.white,
    textSelectionTheme: TextSelectionThemeData(cursorColor: primaryColor),
    appBarTheme: const AppBarTheme(backgroundColor: Colors.black),
    brightness: Brightness.dark,
    primaryColor: primaryColor,
  ),
};
