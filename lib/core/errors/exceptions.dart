class OfflineException implements Exception {}

class ServerException implements Exception {
  final String message;

  ServerException({required this.message});
}

class CacheException implements Exception {}

class InternalException implements Exception {
  final String message;

  InternalException({required this.message});
}
