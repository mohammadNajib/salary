class Failure {
  final String? message;
  Failure({this.message});
}

class OfflineFailure extends Failure {
  OfflineFailure() : super(message: 'لا يتوفر اتصال جيد بالإنترنت، حاول مرة أخرى');
}

class ServerFailure extends Failure {
  ServerFailure({message}) : super(message: message);
}

class CacheFailure extends Failure {
  CacheFailure();
}

class InternalFailure extends Failure {
  InternalFailure();
}
